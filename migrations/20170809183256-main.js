'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
      return queryInterface.createTable('money',
          {
            id: {type: Sequelize.INTEGER, primaryKey: true, allowNull: false, autoIncrement: true},
            count: Sequelize.FLOAT(11, 2)
          });
  },

  down: function (queryInterface, Sequelize) {
      return queryInterface.dropTable('money');
  }
};
