'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
      return queryInterface.sequelize.query("INSERT INTO money SET count=1000");
  },

  down: function (queryInterface, Sequelize) {
      return queryInterface.sequelize.query("TRUNCATE TABLE money");
  }
};
