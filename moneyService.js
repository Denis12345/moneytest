/**
 * Created by Denis on 10.08.2017.
 */
const models = require('./models'),
    winston = require('winston'),
    Money = models['money'];
const moneyService = {
    async sub(token, sum) {
        winston.log('debug', `Money service (sub) params: token=${token}, count=${sum}`);
        sum = +sum;
        if(!token || sum <= 0) {
            winston.log('error', `Money service (sub) params: token=${token}, count=${sum}`);
            throw new Error(`Set params token: ${token}, sum: ${sum}`);
        }

        return models.sequelize.transaction(async (t) => {
            let row = await Money.findById(1, {
                transaction: t,
                lock: t.LOCK.UPDATE
            });

            if(!row.count || row.count < sum) throw new Error('Count is less');
            row.count -= sum;

            await row.save({transaction: t});

            if(process.env.NODE_ENV == 'development'){
                winston.log('debug', `Money service (sub) type: save; params: token=${token}, count=${row.count}`);
            }

            return row.count;
        }).catch((e) => {
            winston.log('error', `Money service (sub) params: token=${token}, count=${sum}`);
            throw e;
        });
    },
    async add(token, sum){
        winston.log('debug', `Money service (add) params: token=${token}, count=${sum}`);
        if(!token || sum <= 0) throw new Error(`Set params token: ${token}, sum: ${sum}`);

        let row = await Money.findById(1);

        row.count += +sum;
        await row.save();

        if(process.env.NODE_ENV == 'development'){
            winston.log('debug', `Money service (add) type: save; params: token=${token}, count=${row.count}`);
        }

        return row.count;
    }
};

module.exports = moneyService;