/**
 * Created by Denis on 09.08.2017.
 */
module.exports = function (Sequelize, DataTypes) {
    return Sequelize.define('money', {
        id: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
        count: DataTypes.FLOAT(11, 2)
    });
};