'use strict';
var http = require('http');
var express = require('express');
var app = express();
const cookieParser = require('cookie-parser'),
    moneyService = require('./moneyService'),
    winston = require('winston');;

/**
 * Get port from environment and store in Express.
 */
var port = normalizePort(process.env.PORT || '3000');


app.use(cookieParser());

/**
 * Функуия иницилизация для выдачи токена
 */
app.use('/init', (req, res) => {
    res.cookie('token', Math.round(Math.random() * 10 + 1));
    res.send('Ok');
});

app.use('/sub', async (req, res, next) => {
    try{
        if(!req.cookies.token) throw new Error('Token incorrect');
        if(!req.query.sum) throw new Error('Set sum');

        await moneyService.sub(req.cookies.token, req.query.sum);
        res.send('Ok');
    } catch (e){
        next(e);
    }
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    let err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.send(err.toString());
});


app.set('port', port);

/**
 * Create HTTP server.
 */

var server = http.createServer(app);

/**
 * Listen on provided port, on all network interfaces.
 */

server.listen(port);
server.on('error', onError);
server.on('listening', onListening);

/**
 * Event listener for HTTP server "listening" event.
 */
function onListening() {
    var addr = server.address();
    var bind = typeof addr === 'string'
        ? 'pipe ' + addr
        : 'port ' + addr.port;
    winston.log('debug', 'Listening on ' + bind);
}


/**
 * Normalize a port into a number, string, or false.
 */
function normalizePort(val) {
    var port = parseInt(val, 10);

    if (isNaN(port)) {
        // named pipe
        return val;
    }

    if (port >= 0) {
        // port number
        return port;
    }

    return false;
}

/**
 * Event listener for HTTP server "error" event.
 */
function onError(error) {
    if (error.syscall !== 'listen') {
        throw error;
    }

    var bind = typeof port === 'string'
        ? 'Pipe ' + port
        : 'Port ' + port;

    // handle specific listen errors with friendly messages
    switch (error.code) {
        case 'EACCES':
            winston.log('error', bind + ' requires elevated privileges');
            process.exit(1);
            break;
        case 'EADDRINUSE':
            winston.log('error', bind + ' is already in use');
            process.exit(1);
            break;
        default:
            throw error;
    }
}

module.exports = app;
