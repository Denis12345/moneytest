const assert = require('assert'), winston = require('winston'), moneyService = require('../moneyService');


winston.configure({
    transports: [
        new (winston.transports.File)({ filename: __dirname + '/../logs/test-log.log' })
    ]
});
winston.level = 'error';

describe('Тестирование сервиса', function() {
    let money = 0, div = 0;

    describe('Проверка параметров при начислении', function () {
        it('Не верный токен при начислении', function () {
            return Promise.all([moneyService.add(0, 1000).then(() => {
                throw new Error('Ошибка проверки токена');
            }).catch(() => {}), moneyService.add(null, 1000).then(() => {
                throw new Error('Ошибка проверки токена');
            }).catch(() => {})]);
        });
        it('Не правильная сумма при начислении', function () {
            return Promise.all([moneyService.add(1, 0).then(() => {
                throw new Error('Ошибка суммы');
            }).catch(() => {}), moneyService.add(1, null).then(() => {
                throw new Error('Ошибка суммы');
            }).catch(() => {}),  moneyService.add(1, -20).then(() => {
                throw new Error('Ошибка суммы');
            }).catch(() => {})]);
        });
    });

    describe('Добавление денег', function() {
        it('Добавить изначальное количество денег', function (done) {
            moneyService.add(1, 1000).then((count) => {
                money = count;
                done();
            }).catch(done);
        });
    });

    describe('Проверка параметров при снятии', function () {
        it('Не верный токен при снятии', function () {
            return Promise.all([moneyService.sub(0, 1000).then(() => {
                throw new Error('Ошибка проверки токена');
            }).catch(() => {}), moneyService.add(null, 1000).then(() => {
                throw new Error('Ошибка проверки токена');
            }).catch(() => {})]);
        });
        it('Не правильная сумма при снятии', function () {
            winston.info(`Текущее количество денег для снятия ${money}`);
            return Promise.all([moneyService.sub(1, 0).then(() => {
                throw new Error('Ошибка суммы');
            }).catch(() => {}), moneyService.sub(1, null).then(() => {
                throw new Error('Ошибка суммы');
            }).catch(() => {}), moneyService.sub(1, -100).then(() => {
                throw new Error('Ошибка суммы');
            }).catch(() => {}), moneyService.sub(1, money * 2).then(() => {
                throw new Error('Ошибка суммы');
            }).catch(() => {})]);
        });
    });

    describe('Снятие денег', function () {
        it('Снятие денег меньше остатка', function (done) {
            this.timeout(5000);
            let promises = [];
            div = money / 200;
            winston.info(`Текущее количество денег для снятия ${div}`);
            for(var i = 100; i--;){
                promises.push(moneyService.sub(Math.round(Math.random() * 10 + 1), div).then(count => {money -= div}));
            }
            Promise.all(promises).then(() => {done()}).catch(e => {throw e});
        });

        it('Полное снятие денег', function (done) {
            this.timeout(5000);
            let promises = [], countThrow = 0;
            div = money / 10;
            winston.info(`Текущее количество денег для полного снятия ${div}`);
            for(var i = 100; i--;){
                promises.push(moneyService.sub(Math.round(Math.random() * 10 + 1), div).then(count => {money -= div}).catch(() => {countThrow++}));
            }

            Promise.all(promises).then(() => {
                winston.info(`Возможный остаток в стулчае неудачи ${money}`);
                assert.notEqual(countThrow, 0);
                done();
            }).catch(e => {throw e});
        });
    });
});